package br.edu.up;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Programa {
	
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		//1. Verificar se o driver MySQL est� no projeto.
		Class.forName("com.mysql.jdbc.Driver").newInstance(); 
		
		//2. Obter a conex�o com o banco de dados.
		String url = "jdbc:mysql://localhost:3306/cadastro?"
				+ "user=aluno&password=positivo";
		Connection con = DriverManager.getConnection(url);
		
		//3. Criar o statement e a query de execu��o.
		Statement statement = con.createStatement();
		String sql = "select * from pessoas;";
		ResultSet resultado = statement.executeQuery(sql);
		
		//4. Percorrer o resultset e imprimir os dados.
		while(resultado.next()){
			int id = resultado.getInt("id");
			String nome = resultado.getString("nome");
			System.out.println("ID: " + id + " Nome: " + nome); 
		}
	}
}